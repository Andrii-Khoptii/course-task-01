"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlainTextMiddleware = void 0;
const common_1 = require("@nestjs/common");
let PlainTextMiddleware = class PlainTextMiddleware {
    use(req, res, next) {
        if (req.headers['content-type'] === 'text/plain') {
            let requestBody = '';
            req.on('data', (chunk) => {
                requestBody += chunk.toString();
            });
            req.on('end', () => {
                try {
                    const parsedBody = JSON.parse(requestBody);
                    if (typeof parsedBody === 'number' && !isNaN(parsedBody)) {
                        req.body = parsedBody;
                        next();
                    }
                    else {
                        throw new Error('Invalid request body');
                    }
                }
                catch (error) {
                    res.status(400).send('Invalid request body');
                }
            });
        }
        else {
            res.status(400).send('Invalid content-type');
        }
    }
};
PlainTextMiddleware = __decorate([
    (0, common_1.Injectable)()
], PlainTextMiddleware);
exports.PlainTextMiddleware = PlainTextMiddleware;
//# sourceMappingURL=plainText.middleware.js.map