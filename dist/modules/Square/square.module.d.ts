import { NestModule, MiddlewareConsumer } from '@nestjs/common';
export declare class SquareModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void;
}
