"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SquareModule = void 0;
const common_1 = require("@nestjs/common");
const square_controller_1 = require("./controllers/square.controller");
const square_service_1 = require("./services/square.service");
const plainText_middleware_1 = require("./middlewares/plainText.middleware");
let SquareModule = class SquareModule {
    configure(consumer) {
        consumer.apply(plainText_middleware_1.PlainTextMiddleware).forRoutes('square');
    }
};
SquareModule = __decorate([
    (0, common_1.Module)({
        controllers: [square_controller_1.SquareController],
        providers: [square_service_1.SquareService],
    })
], SquareModule);
exports.SquareModule = SquareModule;
//# sourceMappingURL=square.module.js.map