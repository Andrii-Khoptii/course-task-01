import { ISquare } from '../interfaces/interfaces';
export declare class SquareService {
    squareNumber(number: number): ISquare;
}
