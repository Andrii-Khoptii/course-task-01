import { SquareService } from '../services/square.service';
export declare class SquareController {
    private squareService;
    constructor(squareService: SquareService);
    squareNumber(number: number): import("../interfaces/interfaces").ISquare;
}
