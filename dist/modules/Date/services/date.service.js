"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateService = void 0;
const common_1 = require("@nestjs/common");
const moment = require("moment");
let DateService = class DateService {
    date(year, month, date) {
        if (month.length === 1) {
            month = `0${month}`;
        }
        if (date.length === 1) {
            date = `0${date}`;
        }
        const data = moment(`${date}/${month}/${year}`, 'DD/MM/YYYY', true);
        const date1 = data.toDate().getTime();
        const date2 = new Date(new Date().toLocaleDateString()).getTime();
        const diffTime = Math.abs(date1 - date2);
        const diffDays = Math.round(diffTime / (1000 * 60 * 60 * 24));
        return {
            weekDay: data.format('dddd'),
            isLeapYear: data.isLeapYear(),
            difference: diffDays,
        };
    }
};
DateService = __decorate([
    (0, common_1.Injectable)()
], DateService);
exports.DateService = DateService;
//# sourceMappingURL=date.service.js.map