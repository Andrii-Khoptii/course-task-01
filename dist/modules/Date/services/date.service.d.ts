export declare class DateService {
    date(year: string, month: string, date: string): {
        weekDay: string;
        isLeapYear: boolean;
        difference: number;
    };
}
