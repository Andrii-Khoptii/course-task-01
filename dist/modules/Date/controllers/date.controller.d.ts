import { DateService } from '../services/date.service';
export declare class DateController {
    private dateService;
    constructor(dateService: DateService);
    date(year: string, month: string, day: string): {
        weekDay: string;
        isLeapYear: boolean;
        difference: number;
    };
}
