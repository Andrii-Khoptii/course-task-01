"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateController = void 0;
const common_1 = require("@nestjs/common");
const date_service_1 = require("../services/date.service");
let DateController = class DateController {
    constructor(dateService) {
        this.dateService = dateService;
    }
    date(year, month, day) {
        return this.dateService.date(year, month, day);
    }
};
__decorate([
    (0, common_1.Get)('/:year/:month/:day'),
    __param(0, (0, common_1.Param)('year')),
    __param(1, (0, common_1.Param)('month')),
    __param(2, (0, common_1.Param)('day')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String]),
    __metadata("design:returntype", void 0)
], DateController.prototype, "date", null);
DateController = __decorate([
    (0, common_1.Controller)('date'),
    __metadata("design:paramtypes", [date_service_1.DateService])
], DateController);
exports.DateController = DateController;
//# sourceMappingURL=date.controller.js.map