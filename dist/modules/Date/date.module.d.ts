import { MiddlewareConsumer, NestModule } from '@nestjs/common';
export declare class DateModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void;
}
