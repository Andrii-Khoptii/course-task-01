"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateModule = void 0;
const common_1 = require("@nestjs/common");
const date_controller_1 = require("./controllers/date.controller");
const date_service_1 = require("./services/date.service");
const dateValidation_middleware_1 = require("./middlewares/dateValidation.middleware");
let DateModule = class DateModule {
    configure(consumer) {
        consumer
            .apply(dateValidation_middleware_1.DateValidation)
            .forRoutes({ path: 'date/:year/:month/:day', method: common_1.RequestMethod.GET });
    }
};
DateModule = __decorate([
    (0, common_1.Module)({
        controllers: [date_controller_1.DateController],
        providers: [date_service_1.DateService],
    })
], DateModule);
exports.DateModule = DateModule;
//# sourceMappingURL=date.module.js.map