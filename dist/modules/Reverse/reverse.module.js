"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReverseModule = void 0;
const common_1 = require("@nestjs/common");
const reverse_controller_1 = require("./controllers/reverse.controller");
const reverse_service_1 = require("./services/reverse.service");
const plainText_middleware_1 = require("./middlewares/plainText.middleware");
let ReverseModule = class ReverseModule {
    configure(consumer) {
        consumer.apply(plainText_middleware_1.PlainTextMiddleware).forRoutes('reverse');
    }
};
ReverseModule = __decorate([
    (0, common_1.Module)({
        controllers: [reverse_controller_1.ReverseController],
        providers: [reverse_service_1.ReverseService],
    })
], ReverseModule);
exports.ReverseModule = ReverseModule;
//# sourceMappingURL=reverse.module.js.map