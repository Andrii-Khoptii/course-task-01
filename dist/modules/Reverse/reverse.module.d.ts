import { NestModule, MiddlewareConsumer } from '@nestjs/common';
export declare class ReverseModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void;
}
