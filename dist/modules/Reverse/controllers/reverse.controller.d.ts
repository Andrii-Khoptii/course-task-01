import { ReverseService } from '../services/reverse.service';
export declare class ReverseController {
    private reverseService;
    constructor(reverseService: ReverseService);
    reverse(text: string): string;
}
