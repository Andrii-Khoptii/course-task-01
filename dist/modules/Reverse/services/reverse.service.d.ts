export declare class ReverseService {
    reverse(text: string): string;
}
