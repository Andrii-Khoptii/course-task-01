import { Module } from '@nestjs/common';
import { SquareModule } from './modules/Square/square.module';
import { ReverseModule } from './modules/Reverse/reverse.module';
import { DateModule } from './modules/Date/date.module';

@Module({
  imports: [SquareModule, ReverseModule, DateModule],
})
export class AppModule {}
