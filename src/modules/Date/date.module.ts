import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { DateController } from './controllers/date.controller';
import { DateService } from './services/date.service';
import { DateValidation } from './middlewares/dateValidation.middleware';

@Module({
  controllers: [DateController],
  providers: [DateService],
})
export class DateModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(DateValidation)
      .forRoutes({ path: 'date/:year/:month/:day', method: RequestMethod.GET });
  }
}
