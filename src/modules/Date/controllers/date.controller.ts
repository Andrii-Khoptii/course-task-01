import { Controller, Get, Param } from '@nestjs/common';
import { DateService } from '../services/date.service';

@Controller('date')
export class DateController {
  constructor(private dateService: DateService) {}

  @Get('/:year/:month/:day')
  date(
    @Param('year') year: string,
    @Param('month') month: string,
    @Param('day') day: string,
  ) {
    return this.dateService.date(year, month, day);
  }
}
