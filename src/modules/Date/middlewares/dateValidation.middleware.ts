import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import * as moment from 'moment';

@Injectable()
export class DateValidation implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    const { year, month, day } = req.params;

    const isValidDate = moment(
      `${day}/${month}/${year}`,
      'DD/MM/YYYY',
    ).isValid();

    if (!isValidDate) {
      return res.status(400).send('Invalid date');
    }

    next();
  }
}
