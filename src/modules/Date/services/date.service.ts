import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

@Injectable()
export class DateService {
  date(year: string, month: string, date: string) {
    if (month.length === 1) {
      month = `0${month}`;
    }

    if (date.length === 1) {
      date = `0${date}`;
    }

    const data = moment(`${date}/${month}/${year}`, 'DD/MM/YYYY', true);

    const date1 = data.toDate().getTime();
    const date2 = new Date(new Date().toLocaleDateString()).getTime();

    const diffTime = Math.abs(date1 - date2);

    const diffDays = Math.round(diffTime / (1000 * 60 * 60 * 24));

    return {
      weekDay: data.format('dddd'),
      isLeapYear: data.isLeapYear(),
      difference: diffDays,
    };
  }
}
