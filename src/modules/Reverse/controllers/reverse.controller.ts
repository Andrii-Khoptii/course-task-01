import { Body, Controller, Post } from '@nestjs/common';
import { ReverseService } from '../services/reverse.service';

@Controller('reverse')
export class ReverseController {
  constructor(private reverseService: ReverseService) {}

  @Post()
  reverse(@Body() text: string) {
    return this.reverseService.reverse(text);
  }
}
