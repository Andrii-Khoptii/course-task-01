import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class PlainTextMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    if (req.headers['content-type'] === 'text/plain') {
      let requestBody = '';

      req.on('data', (chunk) => {
        requestBody += chunk.toString();
      });

      req.on('end', () => {
        try {
          req.body = requestBody;
          next();
        } catch (error) {
          console.log('error', error);
          res.status(400).send('Invalid request body');
        }
      });
    } else {
      res.status(400).send('Invalid content-type');
    }
  }
}
