import { Injectable } from '@nestjs/common';

@Injectable()
export class ReverseService {
  reverse(text: string): string {
    return text.split('').reverse().join('');
  }
}
