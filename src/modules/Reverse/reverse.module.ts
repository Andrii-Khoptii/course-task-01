import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { ReverseController } from './controllers/reverse.controller';
import { ReverseService } from './services/reverse.service';
import { PlainTextMiddleware } from './middlewares/plainText.middleware';

@Module({
  controllers: [ReverseController],
  providers: [ReverseService],
})
export class ReverseModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(PlainTextMiddleware).forRoutes('reverse');
  }
}
