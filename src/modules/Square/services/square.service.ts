import { Injectable } from '@nestjs/common';
import { ISquare } from '../interfaces/interfaces';

@Injectable()
export class SquareService {
  squareNumber(number: number): ISquare {
    return { number, square: Math.pow(number, 2) };
  }
}
