import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class PlainTextMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    if (req.headers['content-type'] === 'text/plain') {
      let requestBody = '';

      req.on('data', (chunk) => {
        requestBody += chunk.toString();
      });

      req.on('end', () => {
        try {
          const parsedBody = JSON.parse(requestBody);
          if (typeof parsedBody === 'number' && !isNaN(parsedBody)) {
            req.body = parsedBody;
            next();
          } else {
            throw new Error('Invalid request body');
          }
        } catch (error) {
          res.status(400).send('Invalid request body');
        }
      });
    } else {
      res.status(400).send('Invalid content-type');
    }
  }
}
