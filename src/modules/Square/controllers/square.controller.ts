import { Body, Controller, Post } from '@nestjs/common';
import { SquareService } from '../services/square.service';

@Controller('square')
export class SquareController {
  constructor(private squareService: SquareService) {}

  @Post()
  squareNumber(@Body() number: number) {
    return this.squareService.squareNumber(number);
  }
}
