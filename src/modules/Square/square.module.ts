import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { SquareController } from './controllers/square.controller';
import { SquareService } from './services/square.service';
import { PlainTextMiddleware } from './middlewares/plainText.middleware';

@Module({
  controllers: [SquareController],
  providers: [SquareService],
})
export class SquareModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(PlainTextMiddleware).forRoutes('square');
  }
}
